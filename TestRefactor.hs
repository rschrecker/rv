import Test.HUnit

import Refactor (delete_function)

test1 = 
    let
        input = [
                "bar = 5",
                "",
                "foo =",
                "    let",
                "        x = 5",
                "        y = 6",
                "    in x + y",
                "",
                "baz x = bar + x",
                ""
            ]
        expected = [
                "bar = 5",
                "",
                "baz x = bar + x",
                ""
            ]
    in TestCase (
        assertEqual "for (1, 2)," expected
        $ delete_function "foo" input
    )

tests = TestList [TestLabel "test1" test1]

main = runTestTT tests
