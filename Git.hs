module Git (
    doRefactor,
    checkRefactor
) where

import Data.List (
        dropWhileEnd,
        intercalate,
        sort
    )
import Data.List.Split (splitOn)
import Prelude hiding (readFile)
import System.Directory (
        listDirectory,
        getCurrentDirectory,
        doesDirectoryExist,
        doesFileExist,
        removeFile
    )
import System.IO.Strict (readFile)
import System.FilePath ((</>))
import System.Process (
        readProcess,
        callCommand
    )

import Entity (
        Files,
        FilePatch (Delete, Modify, New),
        Patch,
        Refactor
    )

import MappingEntity (RefactorMapping)

doRefactor :: Refactor -> String -> IO ()
doRefactor refactor commitMessage = do
    -- need to add relevant directory or maybe it JustWorks?
    status <- readProcess "git" ["status", "--porcelain"] []
    if status == ""
    then do
        files <- read'
        write $ refactor files
        callCommand "git add ."
        callCommand $ "git commit -m '" ++ commitMessage ++ "'"
        return ()
    else error "Dirty working tree"

checkRefactor :: RefactorMapping -> IO Bool
checkRefactor refactorMapping = do
    -- this will exit if there are untracked files, not really needed now that
    -- we're using ls-files? it wont list them anyway
    status <- readProcess "git" ["status", "--porcelain"] []
    if status == ""
    then do
        newFiles <- read'
        rawCommitMessage <- currentCommitMessage
        let commitMessage = dropWhileEnd (=='\n') rawCommitMessage
        callCommand "git reset --hard HEAD~1"
        oldFiles <- read'
        callCommand "git reset --hard HEAD@{1}"
        -- is splitOn too naive?
        let patch = (refactorMapping (splitOn " " commitMessage)) oldFiles
        return $ sort newFiles == sort (apply patch oldFiles)
    else error "Dirty working tree"

currentCommitMessage :: IO String
currentCommitMessage = readProcess "git" ["log", "-1", "--pretty=%B"] []

-- probably not quite right, filenames can have spaces in?
fileList :: IO [FilePath]
fileList = do
    gitOutput <- readProcess "git" ["ls-files"] []
    -- check this works if there are no files?
    let relativePaths = init $ splitOn "\n" gitOutput
    currentDirectory <- getCurrentDirectory
    return $ map (currentDirectory </>) relativePaths


read' :: IO Files
-- I think this excludes new line at end of file?
read' = fileList >>= (mapM (\path -> readFile path >>= return . (,) path . lines))

write :: Patch -> IO ()
write = mapM_ $ uncurry writeFilePatch

writeFilePatch :: FilePath -> FilePatch -> IO()
writeFilePatch path Delete = removeFile path
-- TODO add oldContents to `Modify` for safety?
writeFilePatch path (Modify contents) = do
    exists <- doesFileExist path
    if exists
    -- TODO factor out toString :: Contents -> String
    then writeFile path $ intercalate "\n" contents
    else error "Tried to modify non-existant file"
writeFilePatch path (New contents) = do
    exists <- doesFileExist path
    if exists
    then error "Tried to create already existant file"
    else writeFile path $ intercalate "\n" contents

apply :: Patch -> Files -> Files
apply [] = id
apply ((path, patch):xs) = apply xs . applyFilePatch path patch

applyFilePatch :: FilePath -> FilePatch -> Files -> Files
-- TODO 'existant' maybe not the right word here?
-- look into using some kind of exception monad too?
applyFilePatch path Delete files =
    -- TODO factor out
    if path `elem` map fst files
    then filter (\(path', _) -> path /= path') files
    else error "Tried to delete non-existant file"
applyFilePatch path (Modify contents) files =
    if path `elem` map fst files
    then [
            (path', if path == path' then contents else contents')
            | (path', contents') <- files
        ]
    else error "Tried to modify non-existant file"
applyFilePatch path (New contents) files = do
    if path `elem` map fst files
    then error "Tried to create already existant file"
    else (path, contents):files
