module Refactor (
    delete_function,
    moveLinesToNewFile
) where

import Data.List (isPrefixOf)

import Entity (
        FileRefactor,
        Refactor,
        FilePatch (Modify, New),
        getByPath
    )

-- delete a top level function, if it exists
-- doesn't check for usages
-- TODO: delete fixity declaration too...
-- TODO: delete function defined in infix form...
-- FIXME: would also delete a function whose begins with the target functions name...
delete_function :: String -> FileRefactor
delete_function _ [] = []
delete_function name (x:xs)
    | not $ isValidFunctionName name = error "invalid function name"
    | name `isPrefixOf` x =
        delete_function name $ dropWhile (not . isTopLevel)  xs
    | otherwise = x : delete_function name xs

isTopLevel :: String -> Bool
isTopLevel "" = False
isTopLevel (' ':_) = False
isTopLevel _ = True

-- TODO...
isValidFunctionName :: String -> Bool
isValidFunctionName _ = True

moveLinesToNewFile :: FilePath -> Int -> Int -> FilePath -> Refactor
moveLinesToNewFile from_path start_line end_line to_path files =
    [
        (from_path, Modify $ start ++ end),
        (to_path, New middle)
    ]
    where
        -- fixme: full path is expected
        file = getByPath files from_path
        (start, middle_and_end) = splitAt start_line file
        (middle, end) = splitAt (end_line + 1) middle_and_end
