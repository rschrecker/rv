module Entity (
    FileContent,
    Files,
    FilePatch (Delete, Modify, New),
    Patch,
    FileRefactor,
    Refactor,
    getByPath
) where

type FileContent = [String]  -- or just String?
-- avoid allowing duplicate paths
type Files = [(FilePath, FileContent)]
data FilePatch = Delete | Modify FileContent | New FileContent
type Patch = [(FilePath, FilePatch)]
type FileRefactor = FileContent -> FileContent  -- or return FilePatch?
type Refactor = Files -> Patch

getByPath :: Files -> FilePath -> FileContent
getByPath files path = file
    where [file] = [f | (p, f) <- files, p == path]
