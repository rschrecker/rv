import Data.List (intercalate)
import System.Environment (getArgs)

import Entity (
        Refactor
    )
import Git (
        checkRefactor,
        doRefactor
    )
import Refactor (moveLinesToNewFile)
import MappingEntity (RefactorMapping)

main = do
    args <- getArgs
    foo args

foo :: [String] -> IO ()
foo ("refactor":xs) = doRefactor' xs
foo ["verify"] = checkRefactorAndDisplay

doRefactor' :: [String] -> IO()
doRefactor' xs = doRefactor (refactorMapping xs) $ intercalate " " xs

checkRefactorAndDisplay :: IO ()
checkRefactorAndDisplay = do
    result <- checkRefactor refactorMapping
    putStrLn $ if result then "All good" else "!!! BAD !!!"

moveLinesToNewFile' :: [String] -> Refactor
moveLinesToNewFile' [a, b, c, d] = moveLinesToNewFile a (read b) (read c) d

refactorMapping :: RefactorMapping
refactorMapping ("move-lines-to-new-file":xs) = moveLinesToNewFile' xs
