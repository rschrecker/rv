module MappingEntity (RefactorMapping) where

import Entity (Refactor)

type RefactorMapping = [String] -> Refactor
